import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "./products/products.component";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular';
  url = 'http://34.123.200.254/products';
  products: Product[] = [];
  products$ = this.httpClient.get<any>('http://34.123.200.254/products').pipe(
    map(response => response.data)
  );
  constructor(private httpClient: HttpClient)
  {

  }
  ngOnInit(): void {

  }


}
