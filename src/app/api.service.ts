import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  appUrl = 'http://localhost:4200/products';
  constructor(private httpClient: HttpClient) { }

}
