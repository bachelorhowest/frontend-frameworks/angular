import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

export class Product {
  constructor(
    public id: number,
    public name: string,
    public articleID: bigint,
    public price: number,
    public width: number,
    public height: number,
    public description: string
  ) {
  }
}
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
 products$ = this.httpClient.get<any>('http://34.123.200.254/products').pipe(
    map(response => response.data)
  );
  constructor(private httpClient: HttpClient) { }
  ngOnInit(): void {

  }



}
